<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
           <h3>Activate your Orora Communications app account</h3>

            <p>You’re almost there, click or tap on the button below</p>
            <hr>
            <p><?php echo $post->post_content?></p>
        </div>

        <div class="col-sm-12">
            <form method="post">
                <input type="hidden" name="token" value="<?php echo  sanitize_key($_GET['token']) ?>">
                <button type="submit" class="btn btn-primary" style="background-color: #EB3300;border-color:#EB3300;">Activate my account</button>
            </form>
        </div>

    </div>
</div>

<?php get_footer(); ?>
