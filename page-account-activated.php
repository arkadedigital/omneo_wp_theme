<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
           <h3>Account activated</h3>

            <hr>
            <p>Congratulations, your account is active and you can now return to the Orora Communications app. Simply select the ‘Team member log in’ option on the homescreen and you’re good to go!</p>
            <?php echo $post->post_content ?>

        </div>


    </div>
</div>

<?php get_footer(); ?>
