<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
           <h3>Reset your Orora Communications app passcode </h3>

            <hr>
            <p><?php echo $post->post_content?></p>
        </div>

        <div class="col-sm-12">

            <?php if($_GET['msg']) : ?>
                <h4><span class="label label-danger"><?php echo sanitize_text_field($_GET['msg'])?></span></h4>
                <br>
            <?php endif; ?>
            <form method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Please enter a new four digit passcode</label>
                    <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Passcode">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Please re-enter your new four digit passcode</label>
                    <input type="password" name="password-re" class="form-control" id="exampleInputPassword1" placeholder="Passcode (type again)">
                </div>
                <input type="hidden" name="token" value="<?php echo  sanitize_key($_GET['token']) ?>">
                <button type="submit" class="btn btn-primary" style="background-color:#EB3300;border-color:#EB3300;">Update my passcode</button>
            </form>
        </div>

    </div>
</div>

<?php get_footer(); ?>
