<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3>Congratulations</h3>

            <hr>
            <p>Your passcode has been updated</p>
            <?php echo $post->post_content ?>
        </div>


    </div>
</div>

<?php get_footer(); ?>
