<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="container">

    <div style="position:absolute;top:50%;transform:translateY(-50%);text-align:center;">
        <div
            style="font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:96px;">
            Arkade Omneo WP SDK
        </div>
    </div>

</div>

<?php get_footer(); ?>
